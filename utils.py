from recommendations.models import ParameterWeight, Recommendation
from django.contrib import messages
from handbooks.models import Parameter, Chapter, Handbook, Checkpoint


import ast
import pandas as pd
import numpy as np
from copy import deepcopy

def side(s):
  if (s=='Links') | (s=='Left'):
    return 1
  elif (s=='Rechts') | (s=='Right'):
    return 2
  else:
    return 0
    
def ptype(s):
  if (s=='DropDown') | (s=='QComboBox'):
    return 0
  elif (s=='Texteingabe') | (s=='QLineEdit'):
    return 1
  else:
    return None

def nan_value(a,b):
  if np.isnan(a*b):
    return 0
  else:
    return a*b

#Recommendation(models.Model):
#    name = models.CharField(max_length=126)
#    handbook = models.ForeignKey(Handbook, on_delete=models.CASCADE, null=True)
#    lower_treshold = models.PositiveSmallIntegerField(default=1)
#    upper_treshold = models.PositiveSmallIntegerField(default=1)  

#ParameterWeight(models.Model):
#    parameter = models.ForeignKey(Parameter, on_delete=models.CASCADE)
#    weight = models.PositiveSmallIntegerField(default=1)
#    recommendation = models.ForeignKey('Recommendation', null=True, on_delete=models.CASCADE)


def import_handbooks():
    path_xls = "C:\\Users\\Praxis\\Desktop\\Projekte\\03_MDSSS\\MOH_New\\BlueprintExampleChapter.xlsx"
    #sh_omc=pd.read_excel(path_xls,sheet_name="OMC",encoding='utf-8')
    list_h=['OMC','CI','SPE','VOICE']

    for m in list_h:
        print('Now importing '+m)
        sh_omc=pd.read_excel(path_xls,sheet_name=m)
        sh_omc_t=sh_omc.loc[(sh_omc['H1 text']=='MDSSS-Therapieempfehlung') | 
                            (sh_omc['H1 text']=='Empfehlung zusätzliche Therapie:') |
                            (sh_omc['H1 text']=='Empfehlung zusätzliche Behandlung nach MDSSS:') |
                            (sh_omc['H1 text']=='Empfehlung zusätzliche Therapie:'),:]
        sh_omc=sh_omc.loc[(sh_omc['H1 text']!='MDSSS-Therapieempfehlung') & 
                          (sh_omc['H1 text']!='Empfehlung zusätzliche Therapie:') &
                          (sh_omc['H1 text']!='Empfehlung zusätzliche Behandlung nach MDSSS:') &
                          (sh_omc['H1 text']!='Empfehlung zusätzliche Therapie:'),:]
    
        h = Handbook.objects.create(name=m+' v0', draft=True)
        all_chapter=list(sh_omc['H0 time'].drop_duplicates())
        all_rec=sh_omc_t.loc[:,['H0 time','H1 Id','H2 text','Rule']].drop_duplicates().reset_index(drop=True)
        all_rec['lower']=all_rec['Rule'].apply(lambda x:x.replace('>','').split('-')[0])
        all_rec['upper']=all_rec['Rule'].apply(lambda x:x.replace('>','').split('-')[1])
        list_rec=[]
        for i in all_rec.index:
            list_rec.append(Recommendation.objects.create(handbook=h,
                                                          name=all_rec.loc[i,'H2 text'],
                                                          upper_treshold=all_rec.loc[i,'upper'],
                                                          lower_treshold=all_rec.loc[i,'lower']))
        for i in range(len(all_chapter)):
            t = Chapter.objects.create(name=all_chapter[i], handbook=h, index_position=i, draft=True)
            all_checkpoint=sh_omc.loc[sh_omc['H0 time']==all_chapter[i],['H1 text','H1 class','H1 pos','H1 link']].drop_duplicates().reset_index(drop=True)
            for j in all_checkpoint.index:
                if ptype(all_checkpoint.loc[j,'H1 class'])==0:
                    c = Checkpoint.objects.create(text=all_checkpoint.loc[j,'H1 text'],
                                                  side=side(all_checkpoint.loc[j,'H1 pos']),
                                                  chapter=t, 
                                                  parameter_type=ptype(all_checkpoint.loc[j,'H1 class']),
                                                  index_position=j)
                    all_parameters=sh_omc.loc[((sh_omc['H1 text']==all_checkpoint.loc[j,'H1 text']) &
                                (sh_omc['H1 pos']==all_checkpoint.loc[j,'H1 pos'])),
                                ['H1 link','H2 text','H2 value','H2 weight','H2 compliant']].drop_duplicates().reset_index(drop=True)
                    for k in all_parameters.index:
                        p = Parameter.objects.create(parameter_text=str(all_parameters.loc[k,'H2 text']),
                                                     checkpoint=c,
                                                 index_position=k,
                                                 value=nan_value(all_parameters.loc[k,'H2 value'],all_parameters.loc[k,'H2 weight']),
                                                 compliance=bool(all_parameters.loc[k,'H2 compliant']))
                        if ~np.isnan(all_parameters.loc[k,'H1 link']):
                            for l in all_rec.index:
                                if int(all_parameters.loc[k,'H1 link'])==all_rec.loc[l,'H1 Id']:
                                    w = ParameterWeight.objects.create(parameter=p,
                                                                       weight=all_parameters.loc[k,'H2 value']*all_parameters.loc[k,'H2 weight'], 
                                                                       recommendation=list_rec[l])



def import_handbooks_v0_1():
    path_hb  = "moh_handbooks.xlsx"
    path_rec = "moh_rec.xlsx"
    hb=pd.read_excel(path_hb)
    hb['H1 text']=hb['H1 text'].apply(lambda x: x if isinstance(x,str) else '')
    hb['H1 pos'] =hb['H1 pos'].apply(lambda x: x if isinstance(x,str) else 'Normal')
    hb['H2 text']=hb['H2 text'].apply(lambda x: x if isinstance(x,str) else '')
    rec=pd.read_excel(path_rec)
    list_h=list((hb['H0 Chapter']).drop_duplicates())
    list_h.sort()
    list_h=['CRS']
    for m in list_h:
        print('Now importing '+m)
        hb_t=hb.loc[hb['H0 Chapter']==m]
        h = Handbook.objects.create(name=m.split(' ')[0]+' v0', draft=False)
        all_chapter=list(hb_t['H0 time'].drop_duplicates())
        # all_rec=rec.loc[(rec['H0 Chapter']==m.split(' ')[0])]
        # list_rec=[]
        # for i in all_rec.index:
        #     list_rec.append(Recommendation.objects.create(handbook=h,
        #                                                   name=all_rec.loc[i,'H2 text'],
        #                                                   upper_treshold=5,
        #                                                   lower_treshold=10))
        for i in range(len(all_chapter)):
            print('  Now importing Chapter '+all_chapter[i])
            t = Chapter.objects.create(name=all_chapter[i], handbook=h, index_position=i)
            all_checkpoint=hb_t.loc[hb['H0 time']==all_chapter[i],['H1 Id','H1 text','H1 class','H1 pos','H1 var','H1 link']].drop_duplicates().reset_index(drop=True)
            for j in all_checkpoint.index:
                print('    Now importing Checkpoint '+str(j))
                #if it is a ComboBox DropDownmenü
                if ptype(all_checkpoint.loc[j,'H1 class'])==0:
                    c = Checkpoint.objects.create(text=all_checkpoint.loc[j,'H1 text'],
                                                  side=side(all_checkpoint.loc[j,'H1 pos']),
                                                  chapter=t, 
                                                  parameter_type=ptype(all_checkpoint.loc[j,'H1 class']),
                                                  index_position=j)
                    all_parameters=hb_t.loc[((hb['H1 text']==all_checkpoint.loc[j,'H1 text']) &
                                             (hb_t['H1 var']==all_checkpoint.loc[j,'H1 var'])),
                                            ['H1 link','H1 var','H2 text','H2 value','H2 weight','H2 compliant']].drop_duplicates().reset_index(drop=True)
                    for k in all_parameters.index:
                        print('      Now importing Parameter '+str(k))
                        p = Parameter.objects.create(parameter_text=str(all_parameters.loc[k,'H2 text']),
                                                     checkpoint=c,
                                                     index_position=k,
                                                     value=nan_value(all_parameters.loc[k,'H2 value'],all_parameters.loc[k,'H2 weight']),
                                                     compliance=bool(all_parameters.loc[k,'H2 compliant']))

                        # for l in all_rec.index:
                        #     try:
                        #         if ((all_rec.loc[l,'H2 var']==all_parameters.loc[k,'H1 var']) &
                        #             (all_parameters.loc[k,'H2 value'] in ast.literal_eval(all_rec.loc[l,'H2 counts']))):
                        #             w = ParameterWeight.objects.create(parameter=p,
                        #                                                weight=7, 
                        #                                                recommendation=list_rec[l])
                        #     except:
                        #         pass
